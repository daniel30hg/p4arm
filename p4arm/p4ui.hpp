
#pragma once




#include "ff/ffcompid.hpp"
#include "ff/ffcompcontainer.hpp"
#include "ff/ffcomphierarchy.hpp"
#include "ff/ffinputstate.hpp"


#include "bitboard.hpp"
#include "uirelativepos.hpp"
#include "uidrawable.hpp"





namespace p4ui
{
	struct entity {};
	typedef void(*voidVoidFunction)();
	typedef bool(*clickReleaseFunction)(ff::id<entity>, ff::eventClickRelease);


	/// \brief Component id manager, used to create unique ids
	ff::compidmanager<entity> entityManager;
	/// \brief Component hierarchy, used to store information about which ids are parents of other ids
	ff::comphierarchy<entity> entityHierarchy;
	/// \brief Component container, used to store relative position per id
	ff::compcontainer<entity, component::relativepos> entityRelativePositions;
	/// \brief Component container, used to store drawables (text, rectangles, images...) per id
	ff::compcontainer<entity, component::drawable> entityDrawables;
	/// \brief Component container, used to store click events per id
	ff::compcontainer<entity, clickReleaseFunction> entityEventsClick;


	sf::Font assetFont;
	sf::Texture assetControlRestart;



	/// \brief BOARD: Model of the board, ids for each circle on the board and boolean for allowing the user to edit the board or not
	bool editMode = true;
	bitboard board;
	ff::id<entity> boardIds[7][6];


	/// \brief AMMO: ids and state
	ff::id<entity> ammoIds[2][4];
	ff::dynarray<ff::dynarray<bool>> ammoState;


	/// \brief CAMERA: camera sprite
	ff::id<entity> cameraId;


	/// \brief CONTROL PANEL: camera button
	ff::id<entity> controlCameraId;


	/// \brief STATE: robot state
	ff::id<entity> stateRobotId;
	ff::id<entity> stateProcessingId;



	/// \brief Populate the instance with the connect 4 ui elements
	void initialize();

	/// \brief Update the position and state of all UI elements
	/// \param _windowSize: the current size of the window in pixels
	/// \param _inputState: the current state of the inputs, containing pending events
	void update(ff::vec2u _windowSize, ff::inputstate _inputState = ff::inputstate());

	/// \brief Update with a new camera frame, and the robot & program status
	/// \param _sprite: the sprite of the new camera frame (use cvimage.drawToTexture() to obtain it)
	void update(ff::vec2u _windowSize, sf::Sprite _sprite, bool _robotIsConnected, bool _waitingForPlayer, ff::inputstate _inputState);


	/// \brief Draw all UI elements into the window (WARNING: you MUST call update before calling this function)
	/// \param _window: The window to draw all elements in
	void draw(sf::RenderWindow& _window);
}


void p4ui::initialize()
{
	if (!assetFont.loadFromFile("assets/arial.ttf")) { std::cout << "[!] ERROR: couldn't load font 'assets/arial.ttf'\nPlease copy the asset provided folder next to the executable.\n"; ff::sleep(10000); abort(); }
	if (!assetControlRestart.loadFromFile("assets/restartButton.png")) { std::cout << "[!] ERROR: couldn't load image 'assets/restartButton.png'\nPlease copy the provided asset folder next to the executable.\n"; ff::sleep(10000); abort(); }


	
	ammoState.resize(2);	//
	ammoState[0].resize(4); //
	ammoState[1].resize(4); // Set correct size for ammo


	
	ff::id<entity> idGridTopLeft = entityManager.addNew();																												 //
	entityDrawables.setComponent(idGridTopLeft, component::rect(ff::color::rgb(0, 100, 255, 128)));																		 //
	entityRelativePositions.setComponent(idGridTopLeft, component::relativepos(nValueType::percent, ff::vec2f(0.0f, 0.0f), ff::vec2f(0.2f, 0.4f), nPosSide::topLeft));	 //
																																										 //
	ff::id<entity> idGridTopCenter = entityManager.addNew();																											 //
	entityDrawables.setComponent(idGridTopCenter, component::rect(ff::color::rgb(0, 150, 255, 128)));																	 //
	entityRelativePositions.setComponent(idGridTopCenter, component::relativepos(nValueType::percent, ff::vec2f(0.2f, 0.0f), ff::vec2f(0.6f, 0.4f), nPosSide::topLeft)); //
																																										 //
	ff::id<entity> idGridTopRight = entityManager.addNew();																												 //
	entityDrawables.setComponent(idGridTopRight, component::rect(ff::color::rgb(0, 200, 255, 128)));																	 //
	entityRelativePositions.setComponent(idGridTopRight, component::relativepos(nValueType::percent, ff::vec2f(0.8f, 0.0f), ff::vec2f(0.2f, 0.4f), nPosSide::topLeft));  //
																																										 //
	ff::id<entity> idGridBotLeft = entityManager.addNew();																												 //
	entityDrawables.setComponent(idGridBotLeft, component::rect(ff::color::rgb(0, 100, 196, 128)));																		 //
	entityRelativePositions.setComponent(idGridBotLeft, component::relativepos(nValueType::percent, ff::vec2f(0.0f, 0.4f), ff::vec2f(0.2f, 0.6f), nPosSide::topLeft));	 //
																																										 //
	ff::id<entity> idGridBotCenter = entityManager.addNew();																											 //
	entityDrawables.setComponent(idGridBotCenter, component::rect(ff::color::rgb(0, 150, 196, 128)));																	 //
	entityRelativePositions.setComponent(idGridBotCenter, component::relativepos(nValueType::percent, ff::vec2f(0.2f, 0.4f), ff::vec2f(0.6f, 0.6f), nPosSide::topLeft)); //
																																										 //
	ff::id<entity> idGridBotRight = entityManager.addNew();																												 //
	entityDrawables.setComponent(idGridBotRight, component::rect(ff::color::rgb(0, 200, 196, 128)));																	 //
	entityRelativePositions.setComponent(idGridBotRight, component::relativepos(nValueType::percent, ff::vec2f(0.8f, 0.4f), ff::vec2f(0.2f, 0.6f), nPosSide::topLeft));  // Divide the screen into a grid of 6



	ff::id<entity> idBoardParent = entityManager.addNew();														 //
	entityHierarchy.setParent(idBoardParent, idGridBotCenter);													 //
	entityRelativePositions.setComponent(idBoardParent, component::relativepos(nValueType::ratio, 6.0f / 7.0f)); //
	entityDrawables.setComponent(idBoardParent, component::rect(ff::color::rgb(64, 32, 16, 196)));				 // Add the board background (in top right)


	// Add the 7x6 buttons (in board):
	const float totalCircleSpacingSize = 0.05f;
	const float totalCircleSize = 1.0f - totalCircleSpacingSize;
	for (uint i = 0; i < 7; i += 1)
	{
		for (uint j = 0; j < 6; j += 1)
		{
			ff::id<entity> idCircle = entityManager.addNew();
			entityHierarchy.setParent(idCircle, idBoardParent);
			entityRelativePositions.setComponent(idCircle, component::relativepos(nValueType::percent, ff::vec2f((totalCircleSpacingSize / 8.0f) * (i + 1) + (totalCircleSize / 7.0f) * i, (totalCircleSpacingSize / 7.0f) * (j + 1) + (totalCircleSize / 6.0f) * j), ff::vec2f(totalCircleSize / 7.0f, totalCircleSize / 6.0f), nPosSide::topLeft));
			entityDrawables.setComponent(idCircle, component::circle(ff::color::black()));
			entityEventsClick.setComponent(idCircle,
				[](ff::id<entity> _id, ff::eventClickRelease _event)->bool
				{
					uint x; uint y;
					for (uint k = 0; k < 7; k += 1) { for (uint l = 0; l < 6; l += 1) { if (boardIds[k][l] == _id) { x = k; y = l; } } }

					if (editMode && board.canCycle(x, y)) { board.cycle(x, y); }
					return true;
				}
			);

			boardIds[i][5 - j] = idCircle;
		}
	}


	

	ff::id<entity> idRestart = entityManager.addNew();
	entityHierarchy.setParent(idRestart, idGridTopRight);
	entityRelativePositions.setComponent(idRestart, component::relativepos(nValueType::percent, ff::vec2f(0.2f, 0.2f), ff::vec2f(0.6f, 0.6f), nPosSide::topLeft));
	entityDrawables.setComponent(idRestart, component::sprite(sf::Sprite(assetControlRestart)));
	entityEventsClick.setComponent(idRestart,
		[](ff::id<entity> _id, ff::eventClickRelease _event)->bool //
		{														   //
			board = bitboard();									   // (<- reset the board)
			return true;										   //
		}														   // Define a function for when the restart button is pressed
	);




	ff::id<entity> idAmmoParent = entityManager.addNew();														//
	entityHierarchy.setParent(idAmmoParent, idGridBotLeft);														//
	entityRelativePositions.setComponent(idAmmoParent, component::relativepos(nValueType::ratio, 5.0f / 1.0f)); //
	entityDrawables.setComponent(idAmmoParent, component::rect(ff::color::rgb(64, 32, 16, 196)));				// Add the ammo background (in bot left)

	// Add the ammo slots (in ammo):
	const float totalAmmoSpacingSize = 0.1f;
	const float totalAmmoRectSize = 1.0f - totalAmmoSpacingSize;
	for (uint i = 0; i < 2; i += 1)
	{
		for (uint j = 0; j < 4; j += 1)
		{
			ff::id<entity> idAmmo = entityManager.addNew();
			entityHierarchy.setParent(idAmmo, idAmmoParent);
			entityRelativePositions.setComponent(idAmmo, component::relativepos(nValueType::percent, ff::vec2f((totalAmmoSpacingSize / 3.0f) * (i + 1) + (totalAmmoRectSize / 2.0f) * i, (totalAmmoSpacingSize / 5.0f) * (j + 1) + (totalAmmoRectSize / 4.0f) * j), ff::vec2f(totalAmmoRectSize / 2.0f, totalAmmoRectSize / 4.0f), nPosSide::topLeft));
			entityDrawables.setComponent(idAmmo, component::rect(ff::color::rgb(255, 255, 0, 196)));

			entityEventsClick.setComponent(idAmmo,
				[](ff::id<entity> _id, ff::eventClickRelease _event)->bool
				{
					uint x; uint y;
					for (uint k = 0; k < 2; k += 1) { for (uint l = 0; l < 4; l += 1) { if (ammoIds[k][l] == _id) { x = k; y = l; } } }

					ammoState[x][y] = !ammoState[x][y];
					return true;
				}
			);

			ammoIds[i][j] = idAmmo;
		}
	}





	p4ui::cameraId = entityManager.addNew();																//
	entityHierarchy.setParent(cameraId, idGridTopCenter);													//
	entityRelativePositions.setComponent(cameraId, component::relativepos(nValueType::ratio, 3.0f / 4.0f)); //
	entityDrawables.setComponent(cameraId, component::sprite());											// Add the camera (in top center)


	p4ui::stateRobotId = entityManager.addNew();																								   //
	entityHierarchy.setParent(stateRobotId, idGridTopLeft);																						   //
	entityRelativePositions.setComponent(stateRobotId, component::relativepos(nValueType::px, ff::vec2f(10, 10), ff::vec2f(), nPosSide::topLeft)); //
	entityDrawables.setComponent(stateRobotId, component::text("DISCONNECTED", 12, ff::color::red()));											   // Add the "connected/disconnected" state (in top left)


	p4ui::stateProcessingId = entityManager.addNew();																									//
	entityHierarchy.setParent(stateProcessingId, idGridTopLeft);																						//
	entityRelativePositions.setComponent(stateProcessingId, component::relativepos(nValueType::px, ff::vec2f(10, 30), ff::vec2f(), nPosSide::topLeft)); //
	entityDrawables.setComponent(stateProcessingId, component::text("WAITING FOR PLAYER", 12, ff::color::white()));										// Add the "waiting for player/thinking" state (in top left)

	

}
void p4ui::update(ff::vec2u _windowSize, ff::inputstate _inputState)
{
	// Internal update: events (executes custom code if an item is clicked)
	entityEventsClick.sortByBreadthChildrenFirst(entityHierarchy);
	ff::dynarray<ff::eventClickRelease> clicks = _inputState.getClickReleaseEvents();
	for (uint i = 0; i < clicks.size(); i += 1)
	{
		for (uint j = 0; j < entityEventsClick.size(); j += 1)
		{
			ff::id<entity> id = entityEventsClick.getId(j);
			if (!entityRelativePositions.contains(id)) { ff::log() << "[!] ERROR: ID " << id.value << " has a click event component but no corresponding relative position\n"; continue; }

			component::relativepos pos = entityRelativePositions.get(id);
			if (!pos.bounds.contains(clicks[i].clickPos) || !pos.bounds.contains(clicks[i].releasePos)) { continue; }

			if (entityEventsClick[j](id, clicks[i])) { break; }
		}
	}



	// Internal update: bounds (recalculates the position of each element relative to the window or to other elements)
	entityRelativePositions.sortByBreadthParentsFirst(entityHierarchy);
	for (uint i = 0; i < entityRelativePositions.size(); i += 1)
	{
		ff::id<entity> parentId = entityHierarchy.getParent(entityRelativePositions.getId(i));
		ff::rect<int> parentBounds = ff::rect<int>(0, _windowSize.x, 0, _windowSize.y);
		if (parentId.isValid()) { parentBounds = entityRelativePositions.get(parentId).bounds; }
		entityRelativePositions[i].update(parentBounds);
	}


	// Update the board graphics
	for (uint i = 0; i < 7; i += 1)
	{
		for (uint j = 0; j < 6; j += 1)
		{
			ff::color col = board.getCellColor(i, j);
			if (entityRelativePositions.get(boardIds[i][j]).bounds.contains(_inputState.mousePosition))
			{
				if (board.canCycle(i, j) && editMode) { col.blend(board.getCycleColor(i, j), 0.75f); }
				else { col = ff::color::darkGray(); }
			}

			entityDrawables.setComponent(boardIds[i][j], component::circle(col));
		}
	}

	// Update the ammo graphics
	for (uint i = 0; i < 2; i += 1)
	{
		for (uint j = 0; j < 4; j += 1)
		{
			if (ammoState[i][j]) { entityDrawables.setComponent(ammoIds[i][j], component::rect(ff::color::rgb(255, 255, 0, 196))); }
			else { entityDrawables.setComponent(ammoIds[i][j], component::rect(ff::color::rgb(64, 64, 0, 196))); }
		}
	}

	// Internal update: drawables (sets the previously calculated position of all drawables)
	for (uint i = 0; i < entityDrawables.size(); i += 1)
	{
		ff::id<entity> id = entityDrawables.getId(i);
		if (!entityRelativePositions.contains(id)) { ff::log() << "[!] ERROR: ID " << id.value << " has a drawable component but no corresponding relative position\n"; continue; }

		entityDrawables[i].update(entityRelativePositions.get(id).bounds, assetFont);
	}
}
void p4ui::update(ff::vec2u _windowSize, sf::Sprite _sprite, bool _robotIsConnected, bool _waitingForPlayer, ff::inputstate _inputState)
{
	entityDrawables.get(cameraId) = component::drawable(_sprite); // Update camera sprite

	if (_robotIsConnected) { entityDrawables.get(stateRobotId) = component::text("CONNECTED", 12, ff::color::green()); } //
	else { entityDrawables.get(stateRobotId) = component::text("DISCONNECTED", 12, ff::color::red()); }					 // Update robot "disconnected/connected" text

	if (_waitingForPlayer) { entityDrawables.get(stateProcessingId) = component::text("WAITING FOR PLAYER", 12, ff::color::white()); } //
	else { entityDrawables.get(stateProcessingId) = component::text("THINKING & PLAYING", 12, ff::color::purple()); }				   // Update program "waiting for player/thinking" text

	update(_windowSize, _inputState);
}
void p4ui::draw(sf::RenderWindow& _window)
{
	for (uint i = 0; i < entityDrawables.size(); i += 1) { entityDrawables[i].draw(_window); }
}






