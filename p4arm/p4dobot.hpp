
#pragma once


#include "dobot/DobotDll.h"



#include "ff/ffunistring.hpp"
#include "ff/fftime.hpp"

struct dobot;
struct ptpPos;

enum class nArmStat { connected = 0, disconnected };
enum class nGripStat { released = 0, grabbing, off };


struct ptpPos { float x; float y; float z; float r; ptpPos(float _x, float _y, float _z, float _r) { x = _x; y = _y; z = _z; r = _r; } };

struct dobot
{
	nArmStat status = nArmStat::disconnected;
	ff::unistring port;
	int id = -1;


	uint32_t paramsTimeout = 3000;
	PTPJointParams paramsPTPJoint = { 0 };
	PTPCoordinateParams paramsPTPCoord = { 0 };
	PTPCommonParams paramsPTPCommon = { 0 };
	PTPJumpParams paramsPTPJump = { 0 };
	JOGJointParams paramsJOGJoint = { 0 };
	JOGCoordinateParams paramsJOGCoord = { 0 };
	JOGCommonParams paramsJOGCommon = { 0 };



	dobot();

	/// \brief Attempt to connect to the first dobot found in port list
	/// 
	/// \return True if the connection is succesful, false otherwise
	bool connect();

	/// \brief Disconnect the dobot
	void disconnect();

	/// \brief Check if the robot is connected
	/// 
	/// \return True if the robot is connected, false otherwise
	bool isConnected() const;

	/// \brief Send a command to re-calibrate the robot (the function is blocking until the robot has finished calibrating)
	///
	/// \return True if the command was executed, false otherwise
	bool calibrate();

	/// \brief Ping the robot to check if it is still connected
	///
	/// \return True if the robot is still connected, false otherwise
	bool ping();

	/// \brief Move the robot to a specified position (the function is blocking until the robot has finished moving) (WARNING: make sure the robot is calibrated)
	/// \detail Use getPosition() to obtain position values
	///
	/// \return True if the command was executed, false otherwise
	bool moveTo(ptpPos _pos);

	/// \brief Get the robot's current position
	///
	/// \return The robot's current position
	ptpPos getPosition();

	/// \brief Change the robot's gripper state
	/// \detail The robot will make a lot of noise if the gripper is not off
	///
	/// \param _grip: The state to switch to (release, grabbing, off)
	/// 
	/// \return True if the command was executed, false otherwise
	bool setGrip(nGripStat _grip);


	/// \brief Get the grip state (released, grabbing, off)
	///
	/// \return The gripper's state
	nGripStat getGrip();


	/// \brief Get the position above ammo
	///
	/// \param _x: [0, 1] Which ammo rack to target
	/// 
	/// \return The position
	static ptpPos getHighAmmoPos(uint _x);


	/// \brief Get the position to grab ammo
	///
	/// \param _x: [0, 1] Which ammo rack to target
	/// \param _y: [0, 3] Which specific slot to target
	/// 
	/// \return The position
	static ptpPos getAmmoPos(uint _x, uint _y);

	/// \brief Get the position above the board
	/// 
	/// \return The position
	static ptpPos getHighColumnPos();

	/// \brief Get the position to place into a column
	///
	/// \param _column: [0, 6] The column to target
	/// 
	/// \return The position
	static ptpPos getColumnPos(uint _column);

	/// \brief Get the offset position to "bump" the token into the column
	///
	/// \param _column: [0, 6] The column to target
	/// 
	/// \return The position
	static ptpPos getOffsetColumnPos(uint _column);


private:
	/// \brief Send parameters such as speed and acceleration settings to the dobot
	void configure();

	/// \brief Check if the last command is succesful
	/// \param int _commStat: Number returned by Dobot API functions to represent a response
	/// \return [true: if the command was succesful] [false: otherwise]
	bool checkStat(int _commStat);

	/// \brief Sleep until the command is executed
	/// \param uint64_t _cmdIdx: the id of the command
	/// \return [true: if the command was succesful] [false: otherwise]
	bool sleepUntilCmdExecuted(uint64_t _cmdIdx);
};


dobot::dobot()
{}
bool dobot::connect()
{
	if (isConnected()) { return true; }

	// Detection:
	char dobotSearchStr[1024] = { 0 };
	uint dobotSearchCount = 0;
	dobotSearchCount = SearchDobot(dobotSearchStr, 1024);
	if (dobotSearchCount == 0) { status = nArmStat::disconnected; return false; }


	// Parsing:
	ff::dynarray<ff::regex::match> dobotSearchPorts = ff::unistring(dobotSearchStr).findRegex("[^ ]+");
	if (dobotSearchPorts.size() == 0) { status = nArmStat::disconnected; return false; }


	// Connection:
	status = nArmStat::disconnected;
	for (uint i = 0; i < dobotSearchPorts.size(); i += 1)
	{
		int dobotId = -1;
		int connectStatus = ConnectDobot(std::string(dobotSearchPorts[i].text).c_str(), 115200, nullptr, nullptr, &dobotId);

		if (connectStatus == DobotConnect_NoError)
		{
			port = dobotSearchPorts[i].text;
			id = dobotId;
			status = nArmStat::connected;
			break;
		}
	}

	if (status == nArmStat::disconnected) { return false; }



	// Try to get parameters:
	GetJOGJointParams(id, &paramsJOGJoint);
	GetJOGCoordinateParams(id, &paramsJOGCoord);
	GetJOGCommonParams(id, &paramsJOGCommon);
	GetPTPJointParams(id, &paramsPTPJoint);
	GetPTPCoordinateParams(id, &paramsPTPCoord);
	GetPTPCommonParams(id, &paramsPTPCommon);
	GetPTPJumpParams(id, &paramsPTPJump);

	configure();

	return true;
}
void dobot::disconnect()
{
	DisconnectDobot(id);
	status = nArmStat::disconnected;
}
bool dobot::isConnected() const { return status == nArmStat::connected; }
void dobot::configure()
{
	if (status != nArmStat::connected) { return; }

	// JOG:
	for (uint i = 0; i < 4; i += 1) { paramsJOGJoint.acceleration[i] = 200.0f; paramsJOGJoint.velocity[i] = 200.0f; }
	for (uint i = 0; i < 4; i += 1) { paramsJOGCoord.acceleration[i] = 200.0f; paramsJOGCoord.velocity[i] = 200.0f; }
	paramsJOGCommon.accelerationRatio = 200.0f; paramsJOGCommon.velocityRatio = 200.0f;

	// PTP:
	for (uint i = 0; i < 4; i += 1) { paramsPTPJoint.acceleration[i] = 200.0f; paramsPTPJoint.velocity[i] = 200.0f; }
	paramsPTPCoord.rAcceleration = 200.0f; paramsPTPCoord.xyzAcceleration = 200.0f; paramsPTPCoord.rVelocity = 200.0f; paramsPTPCoord.xyzVelocity = 200.0f;
	paramsPTPCommon.accelerationRatio = 100.0f; paramsPTPCommon.velocityRatio = 100.0f;
	paramsPTPJump.jumpHeight = 10.0f; paramsPTPJump.zLimit = 20.0f;

	int stat;
	uint64_t cmdIdx;

	stat = SetCmdTimeout(id, paramsTimeout); if (!checkStat(stat)) { return; }
	stat = SetJOGJointParams(id, &paramsJOGJoint, true, &cmdIdx); if (!checkStat(stat)) { return; }
	stat = SetJOGCoordinateParams(id, &paramsJOGCoord, true, &cmdIdx); if (!checkStat(stat)) { return; }
	stat = SetJOGCommonParams(id, &paramsJOGCommon, true, &cmdIdx); if (!checkStat(stat)) { return; }
	stat = SetPTPJointParams(id, &paramsPTPJoint, true, &cmdIdx); if (!checkStat(stat)) { return; }
	stat = SetPTPCoordinateParams(id, &paramsPTPCoord, true, &cmdIdx); if (!checkStat(stat)) { return; }
	stat = SetPTPCommonParams(id, &paramsPTPCommon, true, &cmdIdx); if (!checkStat(stat)) { return; }
	stat = SetPTPJumpParams(id, &paramsPTPJump, true, &cmdIdx); if (!checkStat(stat)) { return; }
	stat = SetEndEffectorGripper(id, false, false, true, &cmdIdx); if (!checkStat(stat)) { return; }

	if (!sleepUntilCmdExecuted(cmdIdx)) { disconnect(); return; }

	calibrate();
}
bool dobot::calibrate()
{
	int stat;
	uint64_t cmdIdx;

	HOMECmd home;
	stat = SetHOMECmd(id, &home, true, &cmdIdx); if (!checkStat(stat)) { return false; }
	if (!sleepUntilCmdExecuted(cmdIdx)) { disconnect(); return false; }

	return true;
}
bool dobot::ping()
{
	if (status != nArmStat::connected) { return false; }

	Pose pose;
	if (!checkStat(GetPose(id, &pose))) { disconnect(); return false; }
	return true;
}
// Move to a target position
// [REQUIRES]: dobot.status == ready
// [BLOCKING]: returns when the robot has reached the position
bool dobot::moveTo(ptpPos _pos)
{
	if (status != nArmStat::connected) { return false; }

	PTPCmd cmdPTP;
	cmdPTP.ptpMode = PTPMOVJXYZMode;
	cmdPTP.x = _pos.x;
	cmdPTP.y = _pos.y;
	cmdPTP.z = _pos.z;
	cmdPTP.r = _pos.r;

	
	int stat;
	uint64_t cmdIdx;
	stat = SetPTPCmd(id, &cmdPTP, true, &cmdIdx); if (!checkStat(stat)) { return false; }

	return sleepUntilCmdExecuted(cmdIdx);
}
// Get the robot position
// [REQUIRES]: dobot.status == connected || dobot.status == ready
// [NON-BLOCKING]: the position is returned immediately, even while other commands are running
ptpPos dobot::getPosition()
{
	if (status != nArmStat::connected) { return ptpPos(0, 0, 0, 0); }

	Pose pose;
	if (!checkStat(GetPose(id, &pose))) { return ptpPos(0, 0, 0, 0); }
	return ptpPos(pose.x, pose.y, pose.z, pose.r);
}
// Set the grip
// [REQUIRES]: dobot.status == ready
// [NON-BLOCKING]: returns when the grip has changed
bool dobot::setGrip(nGripStat _grip)
{
	if (status != nArmStat::connected) { return false; }

	int stat;
	uint64_t cmdIdx;
	if (_grip == nGripStat::grabbing) { stat = SetEndEffectorGripper(id, true, true, true, &cmdIdx); if (!checkStat(stat)) { return false; } }
	else if (_grip == nGripStat::released) { stat = SetEndEffectorGripper(id, true, false, true, &cmdIdx); if (!checkStat(stat)) { return false; } }
	else if (_grip == nGripStat::off) { stat = SetEndEffectorGripper(id, false, true, true, &cmdIdx); if (!checkStat(stat)) { return false; } }
	else { return false; }

	bool success = sleepUntilCmdExecuted(cmdIdx);
	ff::sleep(200);
	return success;
}
nGripStat dobot::getGrip()
{
	if (status != nArmStat::connected) { return nGripStat::grabbing; }

	bool gripperEnabled;
	bool gripperStatus;
	GetEndEffectorGripper(id, &gripperEnabled, &gripperStatus);

	if (!gripperEnabled) { return nGripStat::off; }
	else if (gripperStatus) { return nGripStat::grabbing; }
	else { return nGripStat::released; }
}
ptpPos dobot::getHighAmmoPos(uint _x)
{
	if (_x == 0) { return ptpPos(-57.5f, -226.5f, 110.0f, -111.5f); }
	else if (_x == 1) { return ptpPos(-51.0f, 230.0f, 110.0f, 101.5f); }

	std::cout << "[!] ERROR: getHighAmmoPos was given incorrect ammo position\n";
	abort();
}
ptpPos dobot::getAmmoPos(uint _x, uint _y)
{
	if (_x == 0)
	{
		if (_y == 0) { return ptpPos(-57.5f, -226.5f, -110.0f, -111.5f); }
		else if (_y == 1) { return ptpPos(-14.0f, -226.5f, -110.0f, -93.5f); }
		else if (_y == 2) { return ptpPos(29.0f, -226.5f, -110.0f, -83.0f); }
		else if (_y == 3) { return ptpPos(70.5f, -226.5f, -110.0f, -72.5f); }
	}
	else if (_x == 1)
	{
		if (_y == 0) { return ptpPos(-51.0f, 230.0f, -108.0f, 101.5f); }
		else if (_y == 1) { return ptpPos(-9.0f, 230.0f, -108.0f, 92.0f); }
		else if (_y == 2) { return ptpPos(35.0f, 230.0f, -108.0f, 81.0f); }
		else if (_y == 3) { return ptpPos(82.0f, 230.0f, -108.0f, 70.5f); }
	}

	std::cout << "[!] ERROR: getAmmoPos was given incorrect ammo position\n";
	abort();
}
ptpPos dobot::getHighColumnPos()
{
	return ptpPos(231.0f, -7.0f, 150.0f, -9.0f);
}
ptpPos dobot::getColumnPos(uint _column)
{
	if (_column == 0) { return ptpPos(245.4f, -112.0f, 105.5f, -24.5f); }
	else if (_column == 1) { return ptpPos(247.8f, -77.4f, 105.5f, -17.3f); }
	else if (_column == 2) { return ptpPos(247.9f, -43.7f, 105.5f, -10.0f); }
	else if (_column == 3) { return ptpPos(248.5f, -8.5f, 105.5f, -9.0f); }
	else if (_column == 4) { return ptpPos(247.5f, 28.4f, 105.5f, 6.5f); }
	else if (_column == 5) { return ptpPos(246.4f, 60.6f, 105.5f, 13.8f); }
	else if (_column == 6) { return ptpPos(245.0f, 96.9f, 105.5f, 21.6f); }

	std::cout << "[!] ERROR: getColumnPos was given incorrect column number\n";
	abort();
}
ptpPos dobot::getOffsetColumnPos(uint _column)
{
	ptpPos modifiedPos = dobot::getColumnPos(_column);
	modifiedPos.x += 8.0f;
	return modifiedPos;
}
bool dobot::checkStat(int _commStat)
{
	if (_commStat == DobotCommunicate_NoError) { return true; }
	// other values for _commStat: DobotCommunicate_BufferFull, DobotCommunicate_Timeout, DobotCommunicate_InvalidParams
	disconnect();
	return false;
}
bool dobot::sleepUntilCmdExecuted(uint64_t _cmdIdx)
{
	int stat;
	uint64_t currCmdIdx;

	stat = SetQueuedCmdStartExec(id); if (!checkStat(stat)) { return false; }
	stat = GetQueuedCmdCurrentIndex(id, &currCmdIdx); if (!checkStat(stat)) { return false; }
	while (currCmdIdx < _cmdIdx) { stat = GetQueuedCmdCurrentIndex(id, &currCmdIdx); if (!checkStat(stat)) { return false; } }
	stat = SetQueuedCmdStopExec(id); if (!checkStat(stat)) { return false; }

	return true;
}
